package pl.sda.ldz13.spring;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.sda.ldz13.spring.annotation.UserRepositoryAnnotationPrototype;
import pl.sda.ldz13.spring.annotation.UserRepositoryAnnotationSingleton;
import pl.sda.ldz13.spring.config.AppConfig;
import pl.sda.ldz13.spring.repository.UserRepositoryPrototype;
import pl.sda.ldz13.spring.repository.UserRepositorySingleton;
import pl.sda.ldz13.spring.service.UserServicePrototype;
import pl.sda.ldz13.spring.service.UserServiceSingleton;

public class Main {

    private static Logger log = Logger.getLogger(Main.class);

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(AppConfig.class);
        context.refresh();
//        Object object = context.getBean("userRepositoryAnnotationPrototype");
//        Object object = context.getBean("userRepositoryPrototype");

//        exercise1(context);
//        exercise2(context);
//        prototype za kazdym razem tworzy nowy wynik
//        exercise3(context);
        exercise4(context);
        exercise5();
        exercise6();

        context.close();

    }

    private static void exercise6() {

    }

    private static void exercise5() {

    }

    private static void exercise4(AnnotationConfigApplicationContext context) {
        log.info("-----------------exercise4---------------------");

        UserServiceSingleton userServiceSingleton
                = (UserServiceSingleton) context.getBean("userServiceSingleton");

        userServiceSingleton.addUserToPrototype("Janek",34);
        userServiceSingleton.addUserToSingleton("Maciek",27);

        log.info(userServiceSingleton.getUsersNumberFromPrototype());
        log.info(userServiceSingleton.getUsersNumberFromSingleton());

//        tutaj prototype zostal uzyty w userServiceSingleton
//        dlatego nie zostal on zniszczony tylko jest tam trzymany
//        w polu 'userRepositoryAnnotationPrototype' w userServiceSingleton

        userServiceSingleton
                = (UserServiceSingleton) context.getBean("userServiceSingleton");

        userServiceSingleton.addUserToPrototype("Jasiek",34);
        userServiceSingleton.addUserToSingleton("Przemek",27);

        log.info(userServiceSingleton.getUsersNumberFromPrototype());
        log.info(userServiceSingleton.getUsersNumberFromSingleton());
    }

    private static void exercise3(AnnotationConfigApplicationContext context) {
        log.info("-----------------exercise3---------------------");

        UserServicePrototype userServicePrototype
                = (UserServicePrototype) context.getBean("userServicePrototype");

        userServicePrototype.addUserToPrototype("Janek",34);
        userServicePrototype.addUserToSingleton("Maciek",27);

        log.info(userServicePrototype.getUsersNumberFromPrototype());
        log.info(userServicePrototype.getUsersNumberFromSingleton());

    }

    private static void exercise2(AnnotationConfigApplicationContext context) {
        log.info("-----------------exercise2---------------------");

        UserRepositoryAnnotationSingleton userRepositoryAnnotiationSingleton
                = (UserRepositoryAnnotationSingleton) context.getBean("userRepositoryAnnotationSingleton");
        log.info("UserRepSingl get users: " + userRepositoryAnnotiationSingleton.getUserByName("Maniek").getName());

        try {
            UserRepositoryAnnotationPrototype userRepositoryAnnotiationPrototype
                    = (UserRepositoryAnnotationPrototype) context.getBean("userRepositoryAnnotationPrototype");
            log.info("UserRepProt get users: " + userRepositoryAnnotiationPrototype.getUserByName("Cinek").getName());
        } catch (NullPointerException e){
            log.error(e.getMessage());
        }
    }

    private static void exercise1(AnnotationConfigApplicationContext context) {
        log.info("-----------------exercise1---------------------");

        UserServicePrototype userServicePrototype
                = (UserServicePrototype) context.getBean("userServicePrototype");

        userServicePrototype.addUserToSingleton("Maniek", 25);
        userServicePrototype.addUserToPrototype("Cinek", 25);

        log.info("" + userServicePrototype.getUserSingleton("Maniek").getName());
        log.info("" + userServicePrototype.getUserPrototype("Cinek").getName());
    }
}
