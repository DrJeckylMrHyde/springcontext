package pl.sda.ldz13.spring.annotation;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import pl.sda.ldz13.spring.model.User;
import pl.sda.ldz13.spring.repository.UserRepository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;

@Repository
@Scope("prototype")
public class UserRepositoryAnnotationPrototype implements UserRepository {

    private Logger log = Logger.getLogger(UserRepositoryAnnotationPrototype.class);
    private List<User> list;

    public Long addUser(String name, int age) {
        Long id = System.nanoTime();
        User user = new User(id,name,age);
        list.add(user);
        return id;
    }

    public User getUserById(long id) {
        return list.stream()
        .filter(user -> user.getId().equals(id))
                .findAny()
                .orElseThrow(() -> new NullPointerException("User not found"));
    }

    public User getUserByName(String name) {
        return list.stream()
                .filter(user -> user.getName().equals(name))
                .findAny()
                .orElseThrow(() -> new NullPointerException("User not found"));
    }

    public List<User> getAllUsers() {
        return list;
    }


    @PostConstruct
    public void init() {
        list = new ArrayList<User>();
        log.info("Arraylist 'list' initialized in prototype annotation repository");
    }


    @PreDestroy
    public void destroy() {
        log.info("object destroyed in prototype annotation repository");
    }
}
