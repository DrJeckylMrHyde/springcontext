package pl.sda.ldz13.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("pl.sda.ldz13.spring")
//@ComponentScan("pl.sda.ldz13.spring.annotation")
public class AppConfig {

//    @Bean(initMethod = "init", destroyMethod = "destroy")
//    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
//    public UserRepositoryPrototype userRepositoryPrototype(){
//        return new UserRepositoryPrototype();
//    }
//
//    @Bean(initMethod = "init", destroyMethod = "destroy")
//    @Scope(BeanDefinition.SCOPE_SINGLETON)
//    public UserRepositorySingleton userRepositorySingleton(){
//        return new UserRepositorySingleton();
//    }


}
