package pl.sda.ldz13.spring.repository;

import pl.sda.ldz13.spring.model.User;

import java.util.List;

public interface UserRepository {

    Long addUser(String name, int age);

    User getUserById(long id);

    User getUserByName(String name);

    List<User> getAllUsers();

    void init();

    void destroy();

}
