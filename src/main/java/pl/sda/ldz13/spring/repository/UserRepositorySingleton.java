package pl.sda.ldz13.spring.repository;

import pl.sda.ldz13.spring.model.User;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class UserRepositorySingleton implements UserRepository {

    private Logger log = Logger.getLogger(UserRepositorySingleton.class);
    private List<User> list;

    public Long addUser(String name, int age) {
        Long id = System.nanoTime();
        User user = new User(id, name, age);
        list.add(user);
        return id;
    }

    public User getUserById(long id) {
        return list.stream()
                .filter(user -> user.getId().equals(id))
                .findAny()
                .orElseThrow(() -> new NullPointerException("User not found"));
    }

    public User getUserByName(String name) {
        return list.stream()
                .filter(user -> user.getName().equals(name))
                .findAny()
                .orElseThrow(() -> new NullPointerException("User not found"));
    }

    public List<User> getAllUsers() {

        return list;
    }

    public void init() {
        list = new ArrayList<User>();
        log.info("Arraylist 'list' initialized in singleton repository");
    }

    public void destroy() {
        log.info("object destroyed in singleton repository");
    }
}
