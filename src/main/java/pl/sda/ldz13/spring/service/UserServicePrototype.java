package pl.sda.ldz13.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.spring.model.User;
import pl.sda.ldz13.spring.repository.UserRepository;

@Service
@Scope("prototype")
public class UserServicePrototype implements UserService {

    @Autowired
    @Qualifier("userRepositoryAnnotationSingleton")
    private UserRepository userRepositoryAnnotationSingleton;

    @Autowired
    @Qualifier("userRepositoryAnnotationPrototype")
    private UserRepository userRepositoryAnnotationPrototype;

    public void addUserToPrototype(String name, int age) {

        userRepositoryAnnotationPrototype.addUser(name, age);
    }

    public void addUserToSingleton(String name, int age) {

        userRepositoryAnnotationSingleton.addUser(name, age);
    }

    public User getUserPrototype(String name) {

        return userRepositoryAnnotationPrototype.getUserByName(name);
    }

    public User getUserSingleton(String name) {

        return userRepositoryAnnotationSingleton.getUserByName(name);
    }

    public int getUsersNumberFromSingleton() {

        return userRepositoryAnnotationSingleton.getAllUsers().size();
    }

    public int getUsersNumberFromPrototype() {

        return userRepositoryAnnotationPrototype.getAllUsers().size();
    }
}
